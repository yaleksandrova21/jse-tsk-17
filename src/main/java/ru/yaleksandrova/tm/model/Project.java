package ru.yaleksandrova.tm.model;

import ru.yaleksandrova.tm.api.entity.IWBS;
import ru.yaleksandrova.tm.enumerated.Status;
import java.util.UUID;
import java.util.Date;

public class Project implements IWBS {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date startDate;

    private Date finishDate;

    private Date created = new Date();

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString(){
        return id + ": " + name;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

}
