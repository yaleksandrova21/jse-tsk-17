package ru.yaleksandrova.tm.api.sevice;

public interface ILogService {

    void info(String message);

    void command(String message);

    void debug(String message);

    void error(Exception e);

}
