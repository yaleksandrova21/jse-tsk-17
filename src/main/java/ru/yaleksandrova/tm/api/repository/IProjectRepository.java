package ru.yaleksandrova.tm.api.repository;

import ru.yaleksandrova.tm.enumerated.Status;
import ru.yaleksandrova.tm.model.Project;
import java.util.List;
import java.util.Comparator;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll(Comparator<Project> projectComparator);

    void clear();

    int size();

    boolean existsById(String id);

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project startById(String id);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    Project changeStatusByName(String name, Status status);

    List<Project> findAll();
}
