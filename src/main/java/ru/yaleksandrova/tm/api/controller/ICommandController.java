package ru.yaleksandrova.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showHelp();

    void showCommands();

    void showArguments();

    void showCommandValue(String value);

    void showInfo();

    void showErrorCommand();

    void showErrorArgument();

    void exitApplication();

}
